package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;

@Controller
public class PesertaController
{
    @Autowired
    PesertaService pesertaDAO;

    @RequestMapping("/")
    public String index ()
    {
        return "index";
    }

    @RequestMapping("/pengumuman/submit")
    public String pengumumanSubmit (Model model,
            @RequestParam(value = "nomor", required = false) String nomor
    )      
    {
    	PesertaModel peserta = pesertaDAO.selectPeserta (nomor);
    	if(peserta != null){
    		peserta.cekKelulusan();
    		long tempUmur = peserta.hitungUmur();
            peserta.setUmur(tempUmur);
            //System.out.println(peserta.getUniv_accepted().getKode_univ());
    		model.addAttribute ("peserta", peserta);
            return "pengumuman";
    	}else{
    		model.addAttribute ("nomor", nomor);
            return "not-found-peserta";
    	}
    	
    }
    
    @RequestMapping("/peserta/{nomor}")
    public String viewPath (Model model,
            @PathVariable(value = "nomor") String nomor)
    {
        PesertaModel peserta = pesertaDAO.selectPeserta (nomor);
        long tempUmur = peserta.hitungUmur();
        peserta.setUmur(tempUmur);
        
        if (peserta != null) {
            model.addAttribute ("peserta", peserta);
            return "peserta";
        } else {
            model.addAttribute ("nomor", nomor);
            return "not-found-peserta";
        }
    }
	
    @RequestMapping("/univ/peserta/{kode_univ}")
    public String viewAllPeserta (Model model,
            @PathVariable(value = "kode_univ") String kode_univ)
    {
        List<PesertaModel> peserta = pesertaDAO.selectPesertaByUniv(kode_univ);
        model.addAttribute ("peserta", peserta);

        return "univ-allpeserta";
    }

}
