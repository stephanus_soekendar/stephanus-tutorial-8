package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.ProdiModel;
import com.example.service.ProdiService;

@Controller
public class ProdiController
{
    @Autowired
    ProdiService prodiDAO;

    @RequestMapping("/prodi/{kode_prodi}")
    public String viewPath (Model model,
            @PathVariable(value = "kode_prodi") String kode_prodi)
    {
        ProdiModel prodi = prodiDAO.selectProdi (kode_prodi);

        if (prodi != null) {
        	long tempUmurOldest = prodi.getPeserta_oldest().hitungUmur();
        	long tempUmurYoungest = prodi.getPeserta_youngest().hitungUmur();
        	prodi.getPeserta_oldest().setUmur(tempUmurOldest);
        	prodi.getPeserta_youngest().setUmur(tempUmurYoungest);
            model.addAttribute ("prodi", prodi);
            return "prodi";
        } else {
            model.addAttribute ("kode_prodi", kode_prodi);
            return "not-found-prodi";
        }
    }
	
}
