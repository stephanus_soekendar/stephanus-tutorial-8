package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdbTugas01Application
{

    public static void main (String[] args)
    {
        SpringApplication.run (PdbTugas01Application.class, args);
    }
}
