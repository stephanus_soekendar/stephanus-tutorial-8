package com.example.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PesertaModel {
	private String nomor;
	private String nama;
	private Date tgl_lahir;
	private long umur;
	private String kode_prodi;
	private boolean lulus;
	private UnivModel univ_accepted;
	private ProdiModel prodi_accepted;
	
	public long hitungUmur(){
		Date currentYear = new Date();
		long diff = currentYear.getTime() - this.getTgl_lahir().getTime();
		long age = (diff / (24 * 60 * 60 * 1000)); // days
		
		return age / 365;
	}
	
	public void cekKelulusan(){
		if(this.kode_prodi != null){
			this.lulus = true;
		}else{
			this.lulus = false;
		}
		
	}
}
