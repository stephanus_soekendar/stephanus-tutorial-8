package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService
{
    @Autowired
    private PesertaMapper pesertaMapper;


    @Override
    public PesertaModel selectPeserta (String nomor)
    {
        log.info ("select peserta with nomor {}", nomor);
        return pesertaMapper.selectPeserta (nomor);
    }

    public List<PesertaModel> selectPesertaByUniv(String kode_univ){
    	return pesertaMapper.selectPesertaByUniv(kode_univ);
    }
   
}
