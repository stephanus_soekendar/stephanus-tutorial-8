package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UnivMapper;
import com.example.model.UnivModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService
{
    @Autowired
    private UnivMapper univMapper;


    @Override
    public UnivModel selectUniv (String kode_univ)
    {
        log.info ("select universitas with kode {}", kode_univ);
        return univMapper.selectUniv (kode_univ);
    }


    @Override
    public List<UnivModel> selectAllUniv ()
    {
        log.info ("select all universitas");
        return univMapper.selectAllUniv ();
    }

}
