package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;

public interface PesertaService
{
    PesertaModel selectPeserta (String nomor);
    
    List<PesertaModel> selectPesertaByUniv(String kode_univ);
}
