package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Results;

import com.example.model.UnivModel;
import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

@Mapper
public interface ProdiMapper
{
	
	@Select("select a.kode_univ, b.nama_univ, a.kode_prodi, a.nama_prodi, b.url_univ from prodi as A Join univ as B on A.kode_univ = B.kode_univ where kode_prodi = #{kode_prodi}")
	//@Select("select * from prodi where kode_prodi = #{kode_prodi}")
	@Results(value = {
	 @Result(property="kode_univ", column="kode_univ"),
	 @Result(property="nama_univ", column="nama_univ"),
	 @Result(property="url_univ", column="url_univ"),
	 @Result(property="kode_prodi", column="kode_prodi"),
	 @Result(property="nama_prodi", column="nama_prodi"),
	 @Result(property="peserta", column="kode_prodi",
	 javaType = List.class,
	 many=@Many(select="selectPeserta")),
	 @Result(column = "kode_prodi", property = "peserta_oldest", javaType=PesertaMapper.class, one=@One(select="com.example.dao.PesertaMapper.selectOldestPeserta")),
	 @Result(column = "kode_prodi", property = "peserta_youngest", javaType=PesertaMapper.class, one=@One(select="com.example.dao.PesertaMapper.selectYoungestPeserta"))
	})
	ProdiModel selectProdi (@Param("kode_prodi") String kode_prodi); 
    
    @Select("select * from peserta where peserta.kode_prodi = #{kode_prodi}")
    List<PesertaModel> selectPeserta (@Param("kode_prodi") String kode_prodi);
   
}
