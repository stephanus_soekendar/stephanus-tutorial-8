package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Results;

import com.example.model.UnivModel;
import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

@Mapper
public interface UnivMapper
{
	
	@Select("select * from univ where kode_univ = #{kode_univ}")
	@Results(value = {
	 @Result(property="kode_univ", column="kode_univ"),
	 @Result(property="nama_univ", column="nama_univ"),
	 @Result(property="url_univ", column="url_univ"),
	 @Result(property="prodi", column="kode_univ",
	 javaType = List.class,
	 many=@Many(select="selectProdi"))
	})
	UnivModel selectUniv (@Param("kode_univ") String kode_univ); 
	
	@Select("select * from univ")
	@Results(value = {
	 @Result(property="kode_univ", column="kode_univ"),
	 @Result(property="nama_univ", column="nama_univ"),
	 @Result(property="url_univ", column="url_univ")
	 //@Result(property="no_telp_narahubung", column="no_telp_narahubung",
	 //javaType = List.class,
	 //many=@Many(select="selectCourses"))
	})
	List<UnivModel> selectAllUniv ();

    
    @Select("select * from prodi where prodi.kode_univ = #{kode_univ}")
    List<ProdiModel> selectProdi (@Param("kode_univ") String kode_univ);
    
    @Select("select * from univ where univ.kode_univ = (select kode_univ from prodi where prodi.kode_prodi = (select kode_prodi from peserta where peserta.nomor = #{nomor}))")
    UnivModel selectUnivByNomor (@Param("nomor") String nomor);
    
    @Select("select * from univ where univ.kode_univ = (select kode_univ from prodi where prodi.kode_prodi = #{kode_prodi})")
    UnivModel selectUnivByProdi (@Param("kode_prodi") String kode_prodi);
}
