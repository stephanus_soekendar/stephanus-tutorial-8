package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Results;

import com.example.model.PesertaModel;
import com.example.model.UnivModel;

@Mapper
public interface PesertaMapper
{
	
	@Select("select nomor,nama,tgl_lahir,kode_prodi  from peserta where nomor = #{nomor}")
	@Results(value = {
	 @Result(property="nomor", column="nomor"),
	 @Result(property="nama", column="nama"),
	 @Result(property="tgl_lahir", column="tgl_lahir"),
	 @Result(property="kode_prodi", column="kode_prodi"),
	 @Result(column = "nomor", property = "univ_accepted", javaType=UnivMapper.class, one=@One(select="com.example.dao.UnivMapper.selectUnivByNomor")),
	 @Result(column = "kode_prodi", property = "prodi_accepted", javaType=ProdiMapper.class, one=@One(select="com.example.dao.ProdiMapper.selectProdi"))
	})
	PesertaModel selectPeserta (@Param("nomor") String nomor); 
    
	@Select("select * from peserta ORDER BY tgl_lahir LIMIT 1")
    PesertaModel selectOldestPeserta ();
	
	@Select("select * from peserta ORDER BY tgl_lahir DESC LIMIT 1")
    PesertaModel selectYoungestPeserta ();
	
	@Select("select nomor,nama,tgl_lahir,kode_prodi from peserta where kode_prodi IN (select kode_prodi from prodi where prodi.kode_univ = #{kode_univ})")
	@Results(value = {
	 @Result(property="nomor", column="nomor"),
	 @Result(property="nama", column="nama"),
	 @Result(property="tgl_lahir", column="tgl_lahir"),
	 @Result(property="kode_prodi", column="kode_prodi"),
	 @Result(column = "kode_prodi", property = "univ_accepted", javaType=UnivMapper.class, one=@One(select="com.example.dao.UnivMapper.selectUnivByProdi")),
	 @Result(column = "kode_prodi", property = "prodi_accepted", javaType=ProdiMapper.class, one=@One(select="com.example.dao.ProdiMapper.selectProdi"))
	})
	List<PesertaModel> selectPesertaByUniv (@Param("kode_univ") String kode_univ); 
	
	
}
